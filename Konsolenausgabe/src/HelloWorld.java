
public class HelloWorld {
	public static void main(String[] args) {
		
		//beispielsatz
		System.out.print("Das ist ein Beispielsatz. ");
		System.out.print("Ein Beispielsatz ist das.");
		
		//newline characters
		System.out.println("Das ist ein \"Beispielsatz\".");
		System.out.println("Ein Beispielsatz ist das.");
		//print interpretiert zeichen so wie sie sind
		//println fuegt den newlie character an ende einer ausgabe		
		//kommentar
	
		
		//tree
		System.out.println("      * \r\n"
				+ "     *** \r\n"
				+ "    ***** \r\n"
				+ "   ******* \r\n"
				+ "  ********* \r\n"
				+ " *********** \r\n"
				+ "************* \r\n"
				+ "     *** \r\n"
				+ "     *** ");
		
		
			
		System.out.printf( "%.2f\n" , 22.4234234);
		System.out.printf( "%.2f\n" , 111.2222);
		System.out.printf( "%.2f\n" , 4.0);
		System.out.printf( "%.2f\n" , 1000000.551);
		System.out.printf( "%.2f\n" , 97.34);
	}

}

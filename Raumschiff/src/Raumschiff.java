import java.util.ArrayList;

public class Raumschiff {
	
	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private String schiffsname;
	private ArrayList<String> broadcastKommunikator;
	private ArrayList<String> ladungsverzeichnis;
	

	public Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent, int schildeInProzent,
			int huelleInProzent, int lebenserhaltungssystemeInProzent, int androidenAnzahl, String schiffsname,
			ArrayList<String> broadcastKommunikator, ArrayList<String> ladungsverzeichnis) {
		
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = schildeInProzent;
		this.huelleInProzent = huelleInProzent;
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
		this.androidenAnzahl = androidenAnzahl;
		this.schiffsname = schiffsname;
		this.broadcastKommunikator = broadcastKommunikator;
		this.ladungsverzeichnis = ladungsverzeichnis;
	}


	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}


	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}


	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}


	public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}


	public int getSchildeInProzent() {
		return schildeInProzent;
	}


	public void setSchildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}


	public int getHuelleInProzent() {
		return huelleInProzent;
	}


	public void setHuelleInProzent(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}


	public int getLebenserhaltungssystemeInProzent() {
		return lebenserhaltungssystemeInProzent;
	}


	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}


	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}


	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}


	public String getSchiffsname() {
		return schiffsname;
	}


	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}


	public ArrayList<String> getBroadcastKommunikator() {
		return broadcastKommunikator;
	}



	public void setBroadcastKommunikator(ArrayList<String> broadcastKommunikator) {
		this.broadcastKommunikator = broadcastKommunikator;
	}


	public ArrayList<String> getLadungsverzeichnis() {
		return ladungsverzeichnis;
	}


	public void setLadungsverzeichnis(ArrayList<String> ladungsverzeichnis) {
		this.ladungsverzeichnis = ladungsverzeichnis;
	}
	
	private void treffer(Raumschiff r)
	{
		
	}
	
	public void nachrichtAnAlle(String message)
	{
		
	}
	
	public ArrayList<String> eintraegeLogbuchZurueckgeben()
	{
		return broadcastKommunikator;
		
	}
	
	public void photonentorpedosLaden(int anzahlTorpedos)
	{
		
	}
	
	public void reparaturDurchfuehren(boolean schutzschilde, boolean energieversorgung,
			boolean schiffshuelle, int anzahlDroiden)
	{
		
	}
	
	public void zustandRaumschiff() {}
	
	public void ladungsverzeichnisAusgeben() {}
	
	public void ladungsverzeichnisAufraeumen() {}
}

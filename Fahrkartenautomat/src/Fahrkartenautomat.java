﻿import java.util.Scanner;


class Fahrkartenautomat
{
    public static void main(String[] args) {

        double eingezahlterGesamtbetrag;
        double zuZahlenderBetrag = fahrkartenbestellungErfassen();

       // Geldeinwurf
       // -----------
        eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);

       // Fahrscheinausgabe
       // -----------------
        fahrkartenAusgeben();

       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
        rueckgeldAusgeben(eingezahlterGesamtbetrag - zuZahlenderBetrag);

       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt.");
    }

    private static void rueckgeldAusgeben(double rückgabebetrag) {
        if(rückgabebetrag > 0.0)
        {
            System.out.printf("Der Rückgabebetrag in Höhe von %.2f Euro\n", Math.round(rückgabebetrag * 100) / 100.0);
            System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
            {
                muenzeAusgeben(2, "Euro");
                rückgabebetrag -= 2.0;
            }
            while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
            {
                muenzeAusgeben(1, "Euro");
                rückgabebetrag -= 1.0;
            }
            while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
            {
                muenzeAusgeben(50, "Cent");
                rückgabebetrag -= 0.5;
            }
            while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
            {
                muenzeAusgeben(20, "Cent");
                    rückgabebetrag -= 0.2;
            }
            while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
            {
                muenzeAusgeben(10, "Cent");
                rückgabebetrag -= 0.1;
            }
            while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
            {
                muenzeAusgeben(5, "Cent");
                    rückgabebetrag -= 0.05;
            }
        }
    }

    private static void muenzeAusgeben(int betrag, String einheit) {
        System.out.println(betrag + " " + einheit);
    }

    private static void fahrkartenAusgeben() {
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++) {
           System.out.print("=");
           warte(250);
        }
        System.out.println("\n\n");
    }

    private static double fahrkartenBezahlen(double zuZahlenderBetrag) {
        Scanner tastatur = new Scanner(System.in);
        double eingezahlterGesamtbetrag = 0d, eingeworfeneMünze;
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
            System.out.printf("Noch zu zahlen: %.2f Euro\n", Math.round((zuZahlenderBetrag - eingezahlterGesamtbetrag) * 100) / 100.0);
            System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
            eingeworfeneMünze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneMünze;
        }
        return eingezahlterGesamtbetrag;
    }

    private static double fahrkartenbestellungErfassen() {
        Scanner tastatur = new Scanner(System.in);

        double zuZahlenderBetrag;
        short anzahlFahrkarten;

        System.out.print("Zu zahlender Betrag (EURO): ");
        zuZahlenderBetrag = tastatur.nextDouble();
        System.out.print("Anzahl Fahrkarten: ");
        anzahlFahrkarten = tastatur.nextShort();
        zuZahlenderBetrag *= anzahlFahrkarten;

        return zuZahlenderBetrag;

    }

    private static void warte(int millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) { e.printStackTrace(); }
    }
   
}
//(in atom ide gearbeitet)